import home from './home';


const setRoutes = (app) => (
  app
    .use('/home', home)
);

export default setRoutes;