import React, { Component } from 'react';

// STATELESS
const CustomComponent = ({ spanValue, buttonText }) => (
    <div>
        <span> {spanValue} </span>
        <button> {buttonText} </button>
    </div>
);

// STATEFUL
class StatefulComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            h1Value: '',
        }
        this.changeInputValue = this.changeInputValue.bind(this);
    }

    changeInputValue(value) {
        this.setState({ h1Value: value })
    }

    render() {
        return (
            <div>
                <h1> Initial H1 VAlUE: {this.state.h1Value} </h1>
                <input type="text" onChange={e => this.changeInputValue(e.target.value)} />
                <CustomComponent spanValue="asdasd" buttonText="button" />
            </div>
        )
    }
}

export default StatefulComponent;