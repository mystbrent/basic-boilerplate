import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
// import App from './App';
import Login from './SampleLogin';

render(
  <Login />,
  document.getElementById('mount-point'),
);
