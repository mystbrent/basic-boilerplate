import React from 'react';
import Cards from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';

const Exported = () => (
    <Cards
      number={'1234567812345678'}
      name={'Paulo Karlos'}
      expiry={'04/25'}
      cvc={'512'}
    />
);

export default Exported;