import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';

import allRoutes from './routes/';
const app = express();
const port = 3000;

app
  .use(express.static(path.join('public')))
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: false }))
  .set("views", path.join(__dirname, "public/index.html"))

allRoutes(app);
app.listen(port, err => {
  if (err) {
    return console.error(err);
  }
  console.log(`Listening to port: ${port}`);
});
