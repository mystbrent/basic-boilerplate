# Basic Boilerplate For ExpressJS-ReactJS

## About The Boilerplate
Basic boilerplate showcasing Express.js ReactJS with webpack configuration and hot reload.


## Getting Started
    npm install
    npm run start:dev
    npm run bundle

## Built With

* [ExpressJS](https://expressjs.com/) - Node framework
* [ReactJS](https://reactjs.org/) - Frontend framework


## Authors

* **Brent Anthony Tudas** - *Boilerplate Creator* - [mystbrent](https://gitlab.com/mystbrent)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

